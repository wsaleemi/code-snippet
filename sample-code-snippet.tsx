import * as React from "react";
import { AppContext, ContextInterface } from "src/AppComponent";
import { connect } from "react-redux";
import Peer from "simple-peer"
import { ContactModel } from "src/models/contact-models";
import StorageUtils from "src/server/utils/storage-utils";
import { setIsOnCall, setIsReceivingCall, setUserCalling, setUserToCall } from "src/actions/rtc";
import { addChatMessages } from "src/server/utils/task-utils";
import { acceptVideoCall, answerUserCall, initiateVideoCall, onCallEndByInitiator, onCallUser, onEndCall, setAcceptedCallFunction, setCallUserFunction, setIsOnCallFunction, setOnCallDelineFunction, setOnCloseWindowFunction, setPeerConnectionFunction, checkIfUserOnCall } from "src/server/utils/chat-utils";
import { updateUserOnCallStatus } from "src/server/utils/contact-utils";

interface Props {
    state: ContextInterface;
    buildingName: string;
    instance: string;
    isCaller: boolean;
    chatId?: string;

    userCallingContact?: string;
    userToCallContact?: string;
    isReceivingCall?: string;
    isOnCall?: boolean;
    ifUserOnCall?: boolean;
    currentUser?: string;

    setIsReceivingCall?: (isReceivingCall: boolean) => void;
    setUserCalling?: (userCallingContact: string) => void;
    setUserToCall?: (userToCall: string) => void;
    setIsOnCall?: (isOnCall: boolean) => void;

    triggerMobileApp?: boolean
}

interface State {
    myStream?: MediaStream;
    userVideoStream?: MediaStream;
    initialStream?: MediaStream;
    myPeer?: any;

    isMuted?: boolean
    isVideoDisabled?: boolean;
    isScreenShared?: boolean;

    isAlreadyOnCall?: boolean;
    isCallDeclined?: boolean;
    isCallAccepted?: boolean;

    callStartTime?: Date;
}

class VideoCallWindow extends React.Component<Props, State> {
    static contextType = AppContext;
    constructor(props: Props) {
        super(props);
        this.state = {
            myStream: undefined,
            userVideoStream: undefined,
            initialStream: undefined,
            myPeer: undefined,
            isAlreadyOnCall: false,
            isCallDeclined: false,
            isMuted: false,
            isVideoDisabled: false,
            isScreenShared: false,
            isCallAccepted: false,
            callStartTime: undefined,
        }

        this.initiateVideoCallCallback = this.initiateVideoCallCallback.bind(this);
        this.acceptedCallCallback = this.acceptedCallCallback.bind(this);
        this.callUserCallback = this.callUserCallback.bind(this);
        this.isOnCallCallback = this.isOnCallCallback.bind(this);
        this.onCallDelineCallback = this.onCallDelineCallback.bind(this);
        this.onCloseWindowCallback = this.onCloseWindowCallback.bind(this);
    }

    componentDidMount() {
        const { isCaller, ifUserOnCall, currentUser } = this.props;
        let user: ContactModel = JSON.parse(StorageUtils.getItem('user'));
        if (currentUser && currentUser != user.contact) {
            // logout the user
            this.props.state.logout();
            return;
        }

        // pass in another prop as a currentUser and compare it to the user saved in the storage utils, if the user is same, contine the calling functionality otherwise logout the user

        let userCallingContact: string = this.props.userCallingContact || localStorage.getItem("userCallingContact");
        let userToCallContact: string = this.props.userToCallContact || localStorage.getItem("userToCallContact");

        userCallingContact = userCallingContact || user.contact;
        userToCallContact = userToCallContact || user.contact;

        setPeerConnectionFunction(this.initiateVideoCallCallback);
        setAcceptedCallFunction(this.acceptedCallCallback);
        setCallUserFunction(this.callUserCallback);
        setIsOnCallFunction(this.isOnCallCallback);
        setOnCallDelineFunction(this.onCallDelineCallback);
        setOnCloseWindowFunction(this.onCloseWindowCallback);
        checkIfUserOnCall(ifUserOnCall);

        this.props.setIsOnCall(true);
        navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then((stream) => {
            this.setState({
                myStream: stream,
                initialStream: stream
            }, () => {
                let video = document.querySelector("video");
                video.srcObject = stream;
                let userCallingContact: string = this.props.userCallingContact || localStorage.getItem("userCallingContact");
                let userToCallContact: string = this.props.userToCallContact || localStorage.getItem("userToCallContact");

                userCallingContact = userCallingContact || user.contact;
                userToCallContact = userToCallContact || user.contact;

                if (isCaller) {
                    this.props.setUserCalling(userCallingContact);
                    this.props.setUserToCall(userToCallContact);

                    initiateVideoCall(userToCallContact, userCallingContact)
                }
                else {
                    acceptVideoCall(userCallingContact, userToCallContact);
                }
            });
        }).catch((err) => {
            console.log("RERERE", err)
        });

        window.addEventListener("beforeunload", async (e) => {
            await this.windowBeforeUnload();
        });
    }

    async windowBeforeUnload() {
        const { isCallAccepted } = this.state;

        let user: ContactModel = JSON.parse(StorageUtils.getItem('user'));
        let userCallingContact: string = this.props.userCallingContact || localStorage.getItem("userCallingContact");
        let userToCallContact: string = this.props.userToCallContact || localStorage.getItem("userToCallContact");

        userCallingContact = userCallingContact || user.contact;
        userToCallContact = userToCallContact || user.contact;

        onEndCall(userCallingContact);
        onEndCall(userToCallContact);
        // await setUserIsOnCall(userCallingContact, false);
        // await setUserIsOnCall(userToCallContact, false);

        if (this.props.chatId) {
            if (isCallAccepted) {
                const callEnd: Date = new Date();
                await addChatMessages(this.props.chatId, user.contact, user.name, `Call started at ${this.state.callStartTime} and ended at ${callEnd}`, "", false, true);
            }
            else {
                await addChatMessages(this.props.chatId, user.contact, user.name, `Missed call at ${new Date()}`, "", false, true);
                let userToCallContact: string = this.props.userToCallContact || localStorage.getItem("userToCallContact");
                onCallEndByInitiator(userToCallContact)
            }
        }
    }

    async closeButtonClick() {
        const { triggerMobileApp } = this.props;
        let userToCallContact: string = this.props.userToCallContact || localStorage.getItem("userCallingContact");

        // const { isCallAccepted } = this.state;
        let user: ContactModel = JSON.parse(StorageUtils.getItem('user'));

        if (user.contact == userToCallContact) {
            userToCallContact = localStorage.getItem("userToCallContact");
        }

        // making user is on call false when the call ends
        await updateUserOnCallStatus(user?.contact, false);
        await updateUserOnCallStatus(userToCallContact, false);


        if (triggerMobileApp) {
            let userCallingContact: string = this.props.userCallingContact || localStorage.getItem("userCallingContact");
            userCallingContact = userCallingContact || user.contact;
            onCallEndByInitiator(userCallingContact);

            this.windowBeforeUnload();
        }
        else {
            window.close();
        }

    }

    initiateVideoCallCallback(USER_TO_CALL, socketId) {
        let user: ContactModel = JSON.parse(StorageUtils.getItem('user'));
        let { userVideoStream } = this.state;
        const peer = new Peer({
            initiator: true,
            trickle: false,
            stream: this.state.myStream
        });
        this.setState({
            myPeer: peer
        }, () => {
            peer.on("signal", (data) => {
                let userCallingContact: string = this.props.userCallingContact || localStorage.getItem("userCallingContact");

                onCallUser(USER_TO_CALL, data, socketId, user.name, userCallingContact, this.props.buildingName);
            });

            peer.on("stream", (stream) => {
                userVideoStream = stream;
                let video2 = document.getElementsByTagName("video")[1];
                video2.srcObject = userVideoStream;
            });

            peer.on('close', () => { console.log("Inside _socket Initiate VC") })
        });
    }

    acceptedCallCallback(signal) {
        let { myPeer } = this.state;
        myPeer.signal(signal);
        this.setState({
            isCallAccepted: true,
            callStartTime: new Date()
        });
    }

    callUserCallback(data: any) {
        const ReceivingCall = data;
        let { userVideoStream } = this.state;

        const peer = new Peer({
            initiator: false,
            trickle: false,
            stream: this.state.myStream
        });
        this.setState({
            myPeer: peer
        });
        peer.on("signal", (data) => {
            answerUserCall(data, ReceivingCall.from);
        });

        peer.on("stream", (stream) => {
            userVideoStream = stream;

            let video2 = document.getElementsByTagName("video")[1];
            video2.srcObject = userVideoStream;

            this.setState({
                isCallAccepted: true
            })
        })

        peer.signal(ReceivingCall.signal)

        peer.on('close', () => { console.log("Inside _socket callUser") })
    }

    isOnCallCallback() {
        const { ifUserOnCall } = this.props
        this.setState({
            isAlreadyOnCall: ifUserOnCall
        })
    }

    onCallDelineCallback() {
        let user: ContactModel = JSON.parse(StorageUtils.getItem('user'));
        this.setState({
            isCallDeclined: true,
        });
        onEndCall(user.contact);
    }

    onCloseWindowCallback() {
        setTimeout(() => {
            window.close();
        }, 1500);
    }

    muteMic() {
        let { initialStream } = this.state;

        initialStream.getAudioTracks().forEach(track => track.enabled = !track.enabled);
    }

    muteCam() {
        let { myStream } = this.state;

        myStream.getVideoTracks().forEach(track => track.enabled = !track.enabled);
    }

    render() {
        const { isAlreadyOnCall, isCallDeclined, isMuted, isVideoDisabled, isScreenShared, myPeer, myStream, isCallAccepted } = this.state;
        const { isCaller } = this.props;
        let userToCallContact: string = this.props.userToCallContact || localStorage.getItem("userToCall_Name");
        return (
            <AppContext.Consumer>
                {state => {
                    return (
                        <>
                            <div className='wrapper' style={{ backgroundColor: 'black', overflow: "hidden" }}>
                                <div className="row center" style={{ position: 'absolute', left: '0', top: '0', right: '0' }}>
                                    <h2 style={{ paddingTop: '20%', color: 'white' }}>{isAlreadyOnCall ? `${userToCallContact} is already on another call` : isCallDeclined ? `${userToCallContact} declined the call` : isCaller ? "Calling " + userToCallContact : "Accepting Call" ? isCallAccepted : ""}</h2>
                                </div>
                                <div className="videocalling" style={{ display: "block", position: "relative", backgroundColor: 'black' }}>
                                    <div style={{ display: "flex", justifyContent: "center", width: "100%" }}>

                                        <video muted style={{ height: "20%", width: "auto", position: "fixed", bottom: "1%", right: "0", marginRight: '1%', borderRadius: '15px', zIndex: 1, cursor: "pointer" }} autoPlay={true} playsInline id="videoElement11"
                                            onClick={() => {
                                                if (isScreenShared) {
                                                    $('#videoElement11').addClass("videoZoomOut")
                                                    $('#videoElement').addClass("videoZoomIn")
                                                }
                                            }}
                                        >
                                        </video>

                                        <video style={isScreenShared ? { width: "inherit" } : { width: "auto", height: "-webkit-fill-available", position: "fixed" }} autoPlay={true} playsInline id="videoElement" onClick={() => {
                                            if (isScreenShared) {
                                                $('#videoElement11').removeClass("videoZoomOut")
                                                $('#videoElement').removeClass("videoZoomIn")
                                            }
                                        }}>
                                        </video>

                                        <div className="btn-group" style={{ position: 'fixed', bottom: '3%', zIndex: 5 }}>

                                            {isScreenShared &&
                                                <button
                                                    onClick={() => {
                                                        // _socket.emit("on screen share", isCaller ? userToCallContact : userCallingContact)
                                                        navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then(stream => {
                                                            let video = document.querySelector("video");
                                                            video.srcObject = stream;
                                                            myPeer.replaceTrack(myStream.getVideoTracks()[0], stream.getVideoTracks()[0], myPeer.streams[0]);
                                                            myStream.getTracks().forEach(track => track.stop());
                                                            this.setState({
                                                                isScreenShared: false,
                                                                myStream: stream
                                                            }, () => {
                                                                if (isVideoDisabled) {
                                                                    this.muteCam()
                                                                }
                                                            });
                                                        });
                                                    }}
                                                    style={{ borderRadius: '50%', backgroundColor: 'red', border: 'none', height: "60px", width: '60px', marginRight: '20px' }}>
                                                    <i className="fas fa-minus-circle" style={{ color: 'gray', fontSize: '20px' }}></i>
                                                </button>
                                            }
                                            {!isScreenShared &&
                                                <button
                                                    onClick={() => {
                                                        // _socket.emit("on screen share", isCaller ? userToCallContact : userCallingContact)
                                                        (navigator.mediaDevices as any).getDisplayMedia({ video: true, audio: true }).then(stream => {
                                                            let video = document.querySelector("video");
                                                            video.srcObject = stream;
                                                            myPeer.replaceTrack(myStream.getVideoTracks()[0], stream.getVideoTracks()[0], myPeer.streams[0]);
                                                            this.setState({
                                                                isScreenShared: true,
                                                                myStream: stream
                                                            });
                                                            stream.getVideoTracks()[0].onended = () => {
                                                                navigator.mediaDevices.getUserMedia({ video: true, audio: true }).then(stream => {
                                                                    let video = document.querySelector("video");
                                                                    video.srcObject = stream;
                                                                    myPeer.replaceTrack(myStream.getVideoTracks()[0], stream.getVideoTracks()[0], myPeer.streams[0]);
                                                                    this.setState({
                                                                        myStream: stream,
                                                                        isScreenShared: false
                                                                    });
                                                                });
                                                            };
                                                        });
                                                    }}
                                                    style={{ borderRadius: '50%', backgroundColor: '#c9c9c9', border: 'none', height: "60px", width: '60px', marginRight: '20px' }}>
                                                    <i className="fas fa-desktop" style={{ color: 'gray', fontSize: '20px' }}></i>
                                                </button>
                                            }
                                            {!isScreenShared &&
                                                <button
                                                    onClick={() => {
                                                        this.setState({
                                                            isVideoDisabled: !this.state.isVideoDisabled
                                                        }, () => {
                                                            this.muteCam();
                                                        });
                                                    }}
                                                    style={{ borderRadius: '50%', backgroundColor: `${isVideoDisabled ? "red" : "#c9c9c9"} `, border: 'none', height: "60px", width: '60px', marginRight: '20px' }}>
                                                    <i className={isVideoDisabled ? "fas fa-video-slash" : "fas fa-video"} style={{ color: 'gray', fontSize: '20px' }}></i>
                                                </button>
                                            }

                                            <button
                                                onClick={() => {
                                                    this.setState({
                                                        isMuted: !this.state.isMuted
                                                    }, () => {
                                                        this.muteMic();
                                                    });
                                                }}
                                                style={{
                                                    borderRadius: '50%', backgroundColor: `${isMuted ? "red" : "#c9c9c9"} `, border: 'none', height: "60px", width: '60px',
                                                    marginRight: '20px'
                                                }}>
                                                <i className={isMuted ? "fas fa-microphone-slash" : "fas fa-microphone"} style={{ color: 'gray', fontSize: '20px' }}></i>
                                            </button>

                                            <button
                                                onClick={async () => {
                                                    this.closeButtonClick();
                                                }}
                                                style={{
                                                    borderRadius: '50%', backgroundColor: 'red', border: 'none', height: "60px", width: '60px',
                                                    marginRight: '20px'
                                                }}>
                                                <i className="fas fa-phone-slash" style={{ color: 'white', fontSize: '20px' }}></i>
                                            </button>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </>
                    )
                }}
            </AppContext.Consumer >
        )
    }
}

const mapStateToProps = (state) => {
    const userCallingContact = state.rtc.userCallingContact;
    const isOnCall = state.rtc.isOnCall;
    const isReceivingCall = state.rtc.isReceivingCall;
    return {
        userCallingContact,
        isOnCall,
        isReceivingCall
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setIsReceivingCall: (isReceivingCall: boolean) => dispatch(setIsReceivingCall(isReceivingCall)),
        setUserCalling: (userCallingContact: string) => dispatch(setUserCalling(userCallingContact)),
        setUserToCall: (userToCall: string) => dispatch(setUserToCall(userToCall)),
        setIsOnCall: (isOnCall: boolean) => dispatch(setIsOnCall(isOnCall))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoCallWindow);
